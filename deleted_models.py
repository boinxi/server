from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func, text
from sqlalchemy import ForeignKey
from models import User, PeulaLike, GameComment, PeulaComment, PeulaView, GameLike, GameView, Game, Peula, db


class DeletedGame(db.Model):
    __tablename__ = "deleted_games"
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255))
    description = db.Column(db.String(500))
    user_id = db.Column(db.Integer)
    likes = db.Column(db.Integer)
    tag = db.Column(db.String(255))
    time = db.Column(db.DateTime, default=func.now())
    og_id = db.Column(db.Integer)
    og_time = db.Column(db.DateTime)

    def serialize(self):
        return {
            'id': self.id,
            'title': self.title,
            'description': self.description,
            'user_id': self.user_id,
            'likes': self.likes,
            'tag': self.tag,
            'time': self.time,
            'og_id': self.og_id,
            'og_time': self.og_time,
        }

    def __eq__(self, other):
        return self.id == other.id

    def __hash__(self):
        return hash(('id', self.id))

    @staticmethod
    def from_model(og: Game):
        return DeletedGame(title=og.title,
                           description=og.description,
                           user_id=og.user_id,
                           likes=og.likes,
                           tag=og.tag,
                           og_id=og.id,
                           og_time=og.time)


class DeletedUser(db.Model):
    __tablename__ = "deleted_users"
    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(255))
    lastname = db.Column(db.String(255))
    photopath = db.Column(db.String(500))
    nickname = db.Column(db.String(500))
    points = db.Column(db.Integer)
    password = db.Column(db.String(255))
    liked_games = db.Column(db.Text)
    creation_time = db.Column(db.DateTime, default=func.now())
    last_connection_time = db.Column(db.DateTime, default=func.now())
    movment = db.Column(db.String(255))
    male = db.Column(db.Boolean)
    phone = db.Column(db.String(255))
    og_id = db.Column(db.Integer)
    og_time = db.Column(db.DateTime)

    def serialize(self):
        return {
            'id': self.id,
            'firstname': self.firstname,
            'lastname': self.lastname,
            'photopath': self.photopath,
            'nickname': self.nickname,
            'points': self.points,
            'password': self.password,
            'liked_games': self.liked_games,
            'movment': self.movment,
            'male': self.male,
            'phone': '0',
            'og_id': self.og_id,
            'og_time': self.og_time,
        }

    def __eq__(self, other):
        return self.id == other.id

    def __hash__(self):
        return hash(('id', self.id))

    @staticmethod
    def from_model(og: User):
        return DeletedUser(
            firstname=og.firstname,
            lastname=og.lastname,
            photopath=og.photopath,
            nickname=og.nickname,
            points=og.points,
            password=og.password,
            liked_games=og.liked_games,
            creation_time=og.creation_time,
            last_connection_time=og.last_connection_time,
            movment=og.movment,
            male=og.male,
            phone=og.phone,
            og_id=og.id,
            og_time=og.time,
        )


class DeletedPeula(db.Model):
    __tablename__ = "deleted_peulas"
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text)
    content = db.Column(db.Text)
    time = db.Column(db.DateTime, default=func.now())
    likes = db.Column(db.Integer, )
    user_id = db.Column(db.Integer, default=1)
    age = db.Column(db.Integer, default=0)
    kids = db.Column(db.Integer, default=0)
    duration = db.Column(db.Integer, default=0)
    targets = db.Column(db.Text)
    supplyItems = db.Column(db.Text, default="")
    supplyCounts = db.Column(db.Text, default="")
    content_delta = db.Column(db.Text)
    targets_delta = db.Column(db.Text)
    og_id = db.Column(db.Integer)
    og_time = db.Column(db.DateTime)

    def serialize(self):
        return {
            'id': self.id,
            'title': self.title,
            'content': self.content,
            'time': self.time,
            'likes': self.likes,
            'user_id': self.user_id,
            'age': self.age,
            'kids': self.kids,
            'duration': self.duration,
            'targets': self.targets,
            'supply_items': self.supplyItems,
            'supply_counts': self.supplyCounts,
            'content_delta': self.content_delta,
            'targets_delta': self.targets_delta,
            'og_id': self.og_id,
            'og_time': self.og_time,
        }

    def __eq__(self, other):
        return self.id == other.id

    def __hash__(self):
        return hash(('id', self.id))

    @staticmethod
    def from_model(og: Peula):
        return DeletedPeula(
            title=og.title,
            content=og.content,
            likes=og.likes,
            user_id=og.user_id,
            age=og.age,
            kids=og.kids,
            duration=og.duration,
            targets=og.targets,
            supplyItems=og.supplyItems,
            supplyCounts=og.supplyCounts,
            content_delta=og.content_delta,
            targets_delta=og.targets_delta,
            og_id=og.id,
            og_time=og.time,
        )


class DeletedGameLike(db.Model):
    __tablename__ = "deleted_game_likes"
    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.Integer)
    game = db.Column(db.Integer, ForeignKey('games.id'))
    time = db.Column(db.DateTime, default=func.now())
    og_id = db.Column(db.Integer)
    og_time = db.Column(db.DateTime)

    def serialize(self):
        return {
            'id': self.id,
            'user': self.user,
            'game': self.game,
            'time': self.time,
            'og_id': self.og_id,
            'og_time': self.og_time,
        }

    @staticmethod
    def from_model(og: GameLike):
        return DeletedGameLike(
            user=og.user,
            game=og.game,
            og_id=og.id,
            og_time=og.time,
        )


class DeletedPeulaLike(db.Model):
    __tablename__ = "deleted_peula_likes"
    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.Integer)
    peula = db.Column(db.Integer, ForeignKey('peulas.id'))
    time = db.Column(db.DateTime, default=func.now())
    og_id = db.Column(db.Integer)
    og_time = db.Column(db.DateTime)

    def serialize(self):
        return {
            'id': self.id,
            'user': self.user,
            'peula': self.peula,
            'time': self.time,
            'og_id': self.og_id,
            'og_time': self.og_time,
        }

    @staticmethod
    def from_model(og: PeulaLike):
        return DeletedPeulaLike(
            user=og.user,
            peula=og.peula,
            og_id=og.id,
            og_time=og.time,
        )


class DeletedGameView(db.Model):
    __tablename__ = "deleted_game_views"
    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.Integer)
    game = db.Column(db.Integer, ForeignKey('games.id'))
    time = db.Column(db.DateTime, default=func.now())
    og_id = db.Column(db.Integer)
    og_time = db.Column(db.DateTime)

    def serialize(self):
        return {
            'id': self.id,
            'game': self.game,
            'user': self.user,
            'time': self.time,
            'og_id': self.og_id,
            'og_time': self.og_time,
        }

    @staticmethod
    def from_model(og: GameView):
        return DeletedGameView(
            user=og.user,
            game=og.game,
            og_id=og.id,
            og_time=og.time,
        )


class DeletedPeulaView(db.Model):
    __tablename__ = "deleted_peula_views"
    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.Integer)
    peula = db.Column(db.Integer, ForeignKey('peulas.id'))
    time = db.Column(db.DateTime, default=func.now())
    og_id = db.Column(db.Integer)
    og_time = db.Column(db.DateTime)

    def serialize(self):
        return {
            'id': self.id,
            'peula': self.peula,
            'user': self.user,
            'time': self.time,
            'og_id': self.og_id,
            'og_time': self.og_time,
        }

    @staticmethod
    def from_model(og: PeulaView):
        return DeletedPeulaView(
            user=og.user,
            peula=og.peula,
            og_id=og.id,
            og_time=og.time,
        )


class DeletedPeulaComment(db.Model):
    __tablename__ = "deleted_peula_comments"
    id = db.Column(db.Integer, primary_key=True)
    peula = db.Column(db.Integer)
    user = db.Column(db.Integer)
    content = db.Column(db.Text)
    likes = db.Column(db.Integer, default=0)
    time = db.Column(db.DateTime, default=func.now())
    og_id = db.Column(db.Integer)
    og_time = db.Column(db.DateTime)

    def serialize(self):
        return {
            'id': self.id,
            'peula': self.peula,
            'user': self.user,
            'content': self.content,
            'likes': self.likes,
            'time': self.time,
            'og_id': self.og_id,
            'og_time': self.og_time,
        }

    @staticmethod
    def from_model(og: PeulaComment):
        return DeletedPeulaComment(
            peula=og.peula,
            user=og.user,
            content=og.content,
            likes=og.likes,
            og_id=og.id,
            og_time=og.time,
        )


class DeletedGameComment(db.Model):
    __tablename__ = "deleted_game_comments"
    id = db.Column(db.Integer, primary_key=True)
    game = db.Column(db.Integer)
    user = db.Column(db.Integer)
    content = db.Column(db.Text)
    likes = db.Column(db.Integer, default=0)
    time = db.Column(db.DateTime, default=func.now())
    og_id = db.Column(db.Integer)
    og_time = db.Column(db.DateTime)

    def serialize(self):
        return {
            'id': self.id,
            'game': self.game,
            'user': self.user,
            'content': self.content,
            'likes': self.likes,
            'time': self.time,
            'og_id': self.og_id,
            'og_time': self.og_time,
        }

    @staticmethod
    def from_model(og: GameComment):
        return DeletedGameComment(
            game=og.game,
            user=og.user,
            content=og.content,
            likes=og.likes,
            og_id=og.id,
            og_time=og.time,
        )

class DeletedGameFav(db.Model):
    __tablename__ = "deleted_game_favs"
    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.Integer)
    game = db.Column(db.Integer, ForeignKey('games.id'))
    time = db.Column(db.DateTime, default=func.now())
    og_id = db.Column(db.Integer)
    og_time = db.Column(db.DateTime)

    def serialize(self):
        return {
            'id': self.id,
            'user': self.user,
            'game': self.game,
            'time': self.time,
            'og_id': self.og_id,
            'og_time': self.og_time,
        }

    @staticmethod
    def from_model(og: GameLike):
        return DeletedGameFav(
            user=og.user,
            game=og.game,
            og_id=og.id,
            og_time=og.time,
        )


class DeletedPeulaFav(db.Model):
    __tablename__ = "deleted_peula_favs"
    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.Integer)
    peula = db.Column(db.Integer, ForeignKey('peulas.id'))
    time = db.Column(db.DateTime, default=func.now())
    og_id = db.Column(db.Integer)
    og_time = db.Column(db.DateTime)

    def serialize(self):
        return {
            'id': self.id,
            'user': self.user,
            'peula': self.peula,
            'time': self.time,
            'og_id': self.og_id,
            'og_time': self.og_time,
        }

    @staticmethod
    def from_model(og: PeulaLike):
        return DeletedPeulaFav(
            user=og.user,
            peula=og.peula,
            og_id=og.id,
            og_time=og.time,
        )