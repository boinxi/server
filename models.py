from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func, text
from sqlalchemy import ForeignKey

db = SQLAlchemy()


class Tag(db.Model):
    __tablename__ = "game_tags"
    id = db.Column(db.Integer, primary_key=True)
    tag = db.Column(db.String(255))
    linked = db.Column(db.String(255))

    def serialize(self):
        return {
            'id': self.id,
            'tag': self.tag,
            'linked': self.linked,
        }


class Game(db.Model):
    __tablename__ = "games"
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255))
    description = db.Column(db.String(500))
    user_id = db.Column(db.Integer)
    likes = db.Column(db.Integer)
    tag = db.Column(db.String(255))
    time = db.Column(db.DateTime, default=func.now())

    def serialize(self):
        return {
            'id': self.id,
            'title': self.title,
            'description': self.description,
            'user_id': self.user_id,
            'likes': self.likes,
            'tag': self.tag,
            'time': self.time,
        }

    def __eq__(self, other):
        return self.id == other.id

    def __hash__(self):
        return hash(('id', self.id))


class User(db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(255))
    lastname = db.Column(db.String(255))
    photopath = db.Column(db.String(500))
    nickname = db.Column(db.String(500))
    points = db.Column(db.Integer)
    password = db.Column(db.String(255))
    liked_games = db.Column(db.Text)
    creation_time = db.Column(db.DateTime, default=func.now())
    last_connection_time = db.Column(db.DateTime, default=func.now())
    movment = db.Column(db.String(255))
    male = db.Column(db.Boolean)
    phone = db.Column(db.String(255))

    def serialize(self):
        return {
            'id': self.id,
            'firstname': self.firstname,
            'lastname': self.lastname,
            'photopath': self.photopath,
            'nickname': self.nickname,
            'points': self.points,
            'password': self.password,
            'liked_games': self.liked_games,
            'movment': self.movment,
            'male': self.male,
            'phone': '0',
        }

    def long_serialize(self):
        return {
            'id': self.id,
            'firstname': self.firstname,
            'lastname': self.lastname,
            'photopath': self.photopath,
            'nickname': self.nickname,
            'points': self.points,
            'password': self.password,
            'liked_games': self.liked_games,
            'creation_time': self.creation_time,
            'last_connection_time': self.last_connection_time,
            'movment': self.movment,
            'male': self.male,
            'phone': self.phone,
        }

    def __eq__(self, other):
        return self.id == other.id

    def __hash__(self):
        return hash(('id', self.id))


class Peula(db.Model):
    __tablename__ = "peulas"
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text)
    content = db.Column(db.Text)
    time = db.Column(db.DateTime)
    likes = db.Column(db.Integer, )
    user_id = db.Column(db.Integer, default=1)
    age = db.Column(db.Integer, default=0)
    kids = db.Column(db.Integer, default=0)
    duration = db.Column(db.Integer, default=0)
    targets = db.Column(db.Text)
    supplyItems = db.Column(db.Text, default="")
    supplyCounts = db.Column(db.Text, default="")
    content_delta = db.Column(db.Text)
    targets_delta = db.Column(db.Text)

    def serialize(self):
        return {
            'id': self.id,
            'title': self.title,
            'content': self.content,
            'time': self.time,
            'likes': self.likes,
            'user_id': self.user_id,
            'age': self.age,
            'kids': self.kids,
            'duration': self.duration,
            'targets': self.targets,
            'supply_items': self.supplyItems,
            'supply_counts': self.supplyCounts,
            'content_delta': self.content_delta,
            'targets_delta': self.targets_delta,
        }

    def serializeShort(self):
        return {
            'id': self.id,
            'title': self.title,
            'time': self.time,
            'likes': self.likes,
            'user_id': self.user_id,
            'age': self.age,
            'kids': self.kids,
            'duration': self.duration,
        }

    def __eq__(self, other):
        return self.id == other.id

    def __hash__(self):
        return hash(('id', self.id))


class GameLike(db.Model):
    __tablename__ = "game_likes"
    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.Integer)
    game = db.Column(db.Integer, ForeignKey('games.id'))
    time = db.Column(db.DateTime, default=func.now())

    def serialize(self):
        return {
            'id': self.id,
            'user': self.user,
            'game': self.game,
            'time': self.time,
        }


class PeulaLike(db.Model):
    __tablename__ = "peula_likes"
    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.Integer)
    peula = db.Column(db.Integer, ForeignKey('peulas.id'))
    time = db.Column(db.DateTime, default=func.now())

    def serialize(self):
        return {
            'id': self.id,
            'user': self.user,
            'peula': self.peula,
            'time': self.time,
        }


class GameView(db.Model):
    __tablename__ = "game_views"
    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.Integer)
    game = db.Column(db.Integer, ForeignKey('games.id'))
    time = db.Column(db.DateTime, default=func.now())

    def serialize(self):
        return {
            'id': self.id,
            'game': self.game,
            'user': self.user,
            'time': self.time,
        }


class PeulaView(db.Model):
    __tablename__ = "peula_views"
    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.Integer)
    peula = db.Column(db.Integer, ForeignKey('peulas.id'))
    time = db.Column(db.DateTime, default=func.now())

    def serialize(self):
        return {
            'id': self.id,
            'peula': self.peula,
            'user': self.user,
            'time': self.time,
        }


class PeulaSearch(db.Model):
    __tablename__ = "peula_searches"
    id = db.Column(db.Integer, primary_key=True)
    search = db.Column(db.Text)
    user = db.Column(db.Integer)
    time = db.Column(db.DateTime, default=func.now())

    def serialize(self):
        return {
            'id': self.id,
            'search': self.search,
            'user': self.user,
            'time': self.time,
        }


class GameReco(db.Model):
    __tablename__ = "game_reco"
    id = db.Column(db.Integer, primary_key=True)
    game = db.Column(db.Integer)
    points = db.Column(db.Integer)
    time = db.Column(db.DateTime, default=func.now())

    def serialize(self):
        return {
            'id': self.id,
            'game': self.game,
            'points': self.points,
            'time': self.time,
        }


class PeulaReco(db.Model):
    __tablename__ = "peula_reco"
    id = db.Column(db.Integer, primary_key=True)
    peula = db.Column(db.Integer)
    points = db.Column(db.Integer)
    time = db.Column(db.DateTime, default=func.now())

    def serialize(self):
        return {
            'id': self.id,
            'peula': self.peula,
            'points': self.points,
            'time': self.time,
        }


class PeulaComment(db.Model):
    __tablename__ = "peula_comments"
    id = db.Column(db.Integer, primary_key=True)
    peula = db.Column(db.Integer)
    user = db.Column(db.Integer)
    content = db.Column(db.Text)
    likes = db.Column(db.Integer, default=0)
    time = db.Column(db.DateTime, default=func.now())

    def serialize(self):
        return {
            'id': self.id,
            'peula': self.peula,
            'user': self.user,
            'content': self.content,
            'likes': self.likes,
            'time': self.time,
        }


class GameComment(db.Model):
    __tablename__ = "game_comments"
    id = db.Column(db.Integer, primary_key=True)
    game = db.Column(db.Integer)
    user = db.Column(db.Integer)
    content = db.Column(db.Text)
    likes = db.Column(db.Integer, default=0)
    time = db.Column(db.DateTime, default=func.now())

    def serialize(self):
        return {
            'id': self.id,
            'game': self.game,
            'user': self.user,
            'content': self.content,
            'likes': self.likes,
            'time': self.time,
        }


class WebCode(db.Model):
    __tablename__ = "web_codes"
    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.Integer)
    code = db.Column(db.Text)
    time = db.Column(db.DateTime, default=func.now())

    def serialize(self):
        return {
            'id': self.id,
            'user': self.user,
            'code': self.code,
            'time': self.time,
        }


class Report(db.Model):
    __tablename__ = "reports"
    id = db.Column(db.Integer, primary_key=True)
    post_id = db.Column(db.Integer)
    game = db.Column(db.Boolean)
    time = db.Column(db.DateTime, default=func.now())
    type = db.Column(db.Integer)
    description = db.Column(db.Text)
    sender_id = db.Column(db.Integer)

    def serialize(self):
        return {
            'id': self.id,
            'post_id': self.post_id,
            'game': self.game,
            'time': self.time,
            'type': self.type,
            'description': self.description,
            'sender_id': self.sender_id,
        }


class UserConnection(db.Model):
    __tablename__ = "user_connections"
    id = db.Column(db.Integer, primary_key=True)
    time = db.Column(db.DateTime, default=func.now())
    user = db.Column(db.Integer)
    device_info = db.Column(db.Text)

    def serialize(self):
        return {
            'id': self.id,
            'time': self.time,
            'user': self.user,
            'device_info': self.device_info,
        }


class GameFav(db.Model):
    __tablename__ = "game_favs"
    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.Integer)
    game = db.Column(db.Integer, ForeignKey('games.id'))
    time = db.Column(db.DateTime, default=func.now())

    def serialize(self):
        return {
            'id': self.id,
            'user': self.user,
            'game': self.game,
            'time': self.time,
        }


class PeulaFav(db.Model):
    __tablename__ = "peula_favs"
    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.Integer)
    peula = db.Column(db.Integer, ForeignKey('peulas.id'))
    time = db.Column(db.DateTime, default=func.now())

    def serialize(self):
        return {
            'id': self.id,
            'user': self.user,
            'peula': self.peula,
            'time': self.time,
        }
