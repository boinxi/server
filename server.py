import base64
import json
from datetime import datetime
from os import listdir, path
from random import randint

import requests
from flask import Flask
from flask import jsonify
from flask import request
from flask import send_file
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func, text
from sqlalchemy.sql.expression import desc
from sqlalchemy import ForeignKey
from models import User, PeulaLike, Tag, GameComment, PeulaComment, PeulaSearch, Report, PeulaView, GameLike, GameView, \
    WebCode, GameReco, PeulaReco, Game, Peula, db, UserConnection, GameFav, PeulaFav
from deleted_models import DeletedPeula, DeletedGame, DeletedGameComment, DeletedGameLike, DeletedGameView, \
    DeletedPeulaComment, DeletedPeulaLike, DeletedPeulaView, DeletedUser, DeletedGameFav, DeletedPeulaFav

app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": "*"}})
app.config['JSON_AS_ASCII'] = False
SQLALCHEMY_DATABASE_URI = "mysql+mysqlconnector://{username}:{password}@{hostname}/{databasename}".format(
    username="boinxi",
    password="Amitvaknin240798",
    hostname="boinxi.mysql.pythonanywhere-services.com",
    databasename="boinxi$my_db",
)
app.config["SQLALCHEMY_DATABASE_URI"] = SQLALCHEMY_DATABASE_URI
app.config["SQLALCHEMY_POOL_RECYCLE"] = 299
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config['UPLOAD_FOLDER'] = "mysite/profile_pics"
db.init_app(app)


########################################################
#                        MISC METHODS                  #
########################################################
def random_with_N_digits(n):
    range_start = 10 ** (n - 1)
    range_end = (10 ** n) - 1
    return randint(range_start, range_end)


########################################################
#                        DB METHODS                    #
########################################################


def getAllGames():
    return Game.query.all()


def findPeulaByKey(word):
    keywords = list(
        filter(lambda x: ((x != "") and (x != " ")), word.split(" ")))
    res = list()
    for keyword in keywords:
        res.extend(list(Peula.query.filter(
            Peula.title.like('%' + keyword + '%'))))
        res.extend(list(Peula.query.filter(
            Peula.targets.like('%' + keyword + '%'))))
        res.extend(list(Peula.query.filter(
            Peula.content.like('%' + keyword + '%'))))
    return set(res)


def findPeulaByKeyQuick(word):
    keywords = list(
        filter(lambda x: ((x != "") and (x != " ")), word.split(" ")))
    res = list()
    for keyword in keywords:
        res.extend(list(Peula.query.filter(
            Peula.title.like('%' + keyword + '%'))))
        res.extend(list(Peula.query.filter(
            Peula.targets.like('%' + keyword + '%'))))
    return set(res)


# #################################################
#                     debug                       #
###################################################


###################################################
#                 FLUSK ROUTES                    #
###################################################


#       S games
############################################

@app.route("/games")
def games():
    return jsonify(list(map(lambda x: x.serialize(), getAllGames())))


@app.route("/game_tags")
def game_tags():
    return jsonify(list(map(lambda x: x.serialize(), Tag.query.all())))


@app.route("/game_likes")
def game_likes():
    return jsonify(list(map(lambda x: x.serialize(), GameLike.query.all())))


@app.route("/games/<tag>")
def gamesInTag(tag):
    all_games = getAllGames()
    filterd = list()
    for game in all_games:
        currGameTAgs = game.tag.split(" ")
        if tag in currGameTAgs:
            filterd.append(game)
    return jsonify(list(map(lambda x: x.serialize(), filterd)))


@app.route("/games_fast/<tag>")
def gamesInTagFast(tag):
    res = Game.query.filter(Game.tag.like('%' + tag + '%'))
    return jsonify(list(map(lambda x: x.serialize(), res)))


@app.route("/games_page/<tag>/<int:page>")
def gamesInTagPage(tag, page):
    games = Game.query.filter(Game.tag.like('%' + tag + '%')).paginate(page, 3, False).items
    return jsonify(list(map(lambda x: x.serialize(), games)))


@app.route("/games_page_sorted/<tag>/<int:sort>/<int:sort_dir>/<int:page>")
def gamesInTagPageSorted(tag, sort, sort_dir, page):
    # sort:     0-time, 1- likes
    # sort_dir: 0-more to less, 1- less to more
    if sort == 0:
        base = Game.time
    else:
        base = Game.likes

    if sort_dir == 0:
        final_filter = base.desc()
    else:
        final_filter = base.asc()

    games = Game.query.filter(Game.tag.like('%' + tag + '%')).order_by(final_filter).paginate(page, 3, False).items
    return jsonify(list(map(lambda x: x.serialize(), games)))


@app.route("/insert/game", methods=['POST'])
def addGame():
    content = request.json
    print("the content is" + str(content))
    title = content['title']
    description = content['description']
    userId = content['user_id']
    tagId = content['tag_id']
    new = Game(title=title, description=description,
               user_id=userId, tag=tagId, likes=0)
    print("amitt:" + str(new) + "|" + str(new.likes))
    db.session.add(new)
    db.session.commit()
    return "ok"


@app.route("/games_in_string/<string:s>")
def games_in_string(s):
    game_ids = list(s.split(" "))
    game_ids = filter(lambda x: x != '', game_ids)
    all = list()
    for game_id in game_ids:
        all.append(Game.query.filter_by(id=game_id).first())
    return jsonify(list(map(lambda x: x.serialize(), all)))


@app.route("/games_in_string_page/<string:games>/<int:page>")
def games_in_string_page(games, page):
    ids = list(games.split(" "))
    ids = filter(lambda x: x != '', ids)
    value = Game.query.filter(Game.id.in_(ids)).paginate(page, 3, False).items
    return jsonify(list(map(lambda x: x.serialize(), value)))


@app.route("/games_in_string")
def games_in_string_empty():
    return jsonify(list())


@app.route("/liked_games_by_user_page/<int:user>/<int:page>")
def get_liked_games_by_user_page(user, page):
    res = Game.query.join(GameLike).filter(GameLike.user == user).order_by(GameLike.time.desc()).paginate(page, 3,
                                                                                                          False).items
    return jsonify(list(map(lambda x: x.serialize(), res)))


@app.route("/faved_games_by_user_page/<int:user>/<int:page>")
def faved_games_by_user_page(user, page):
    res = Game.query.join(GameFav).filter(GameFav.user == user).order_by(GameFav.time.desc()).paginate(page, 3,
                                                                                                       False).items
    return jsonify(list(map(lambda x: x.serialize(), res)))


@app.route("/last_seen_games_by_user_page/<int:user>/<int:page>")
def get_last_seen_games_by_user_page(user, page):
    res = Game.query.join(GameView).filter(GameView.user == user).order_by(GameView.time.desc()).paginate(page, 3,
                                                                                                          False).items
    return jsonify(list(map(lambda x: x.serialize(), res)))


#       S peulas
############################################

@app.route("/peulas")
def peulas():
    return jsonify(list(map(lambda x: x.serialize(), Peula.query.all())))


@app.route("/peula_by_id/<int:id>")
def peulaById(id):
    res = Peula.query.filter(Peula.id == id).first()
    return jsonify(res.serialize())


@app.route("/peula_likes")
def peula_likes():
    return jsonify(list(map(lambda x: x.serialize(), PeulaLike.query.all())))


@app.route("/insert/peula", methods=['POST'])
def addPeula():
    content = request.json
    title = content['title']
    peula_content = content['content']
    userId = content['user_id']
    age = content['age']
    kids = content['kids']
    duration = content['duration']
    targets = content['targets']
    supplyCounts = content['supply_counts']
    supplyItems = content['supply_items']
    peula_content_delta = content['content_delta']
    targets_delta = content['targets_delta']

    new = Peula(title=title, content=peula_content, user_id=userId, likes=0, time=datetime.now(), age=age, kids=kids,
                duration=duration, targets=targets, supplyCounts=supplyCounts, supplyItems=supplyItems,
                content_delta=peula_content_delta, targets_delta=targets_delta)
    db.session.add(new)
    db.session.commit()
    return jsonify("{'status':200}")


@app.route("/peulas_in_string/<string:s>")
def peulas_in_string(s):
    peula_ids = list(s.split(" "))
    peula_ids = filter(lambda x: x != '', peula_ids)
    all = list()
    for peula_id in peula_ids:
        all.append(Peula.query.filter_by(id=peula_id).first())
    return jsonify(list(map(lambda x: x.serialize(), all)))


@app.route("/peulas_in_string_page/<string:peulas>/<int:page>")
def peulas_in_string_page(peulas, page):
    ids = list(peulas.split(" "))
    ids = filter(lambda x: x != '', ids)
    value = Peula.query.filter(Peula.id.in_(ids)).paginate(page, 3, False).items
    return jsonify(list(map(lambda x: x.serialize(), value)))


@app.route("/peulas_in_string")
def peulas_in_string_empty():
    return jsonify(list())


@app.route('/peula/<string:keyword>')
def get_peula_by_Key(keyword):
    return jsonify(list(map(lambda x: x.serialize(), findPeulaByKey(keyword))))


@app.route('/peula_quick_search/<string:keyword>')
def get_peula_by_Key_quick(keyword):
    return jsonify(list(map(lambda x: x.serialize(), findPeulaByKeyQuick(keyword))))


@app.route('/peula/<string:keyword>/short')
def get_peula_by_Key_short(keyword):
    return jsonify(list(map(lambda x: x.serializeShort(), findPeulaByKey(keyword))))


@app.route('/peula_quick_search/<string:keyword>/short')
def get_peula_by_Key_quick_short(keyword):
    return jsonify(list(map(lambda x: x.serializeShort(), findPeulaByKeyQuick(keyword))))


@app.route('/peula_smart_search/<string:keyword>/short')
def get_peula_by_key_smart_short(keyword):
    keyword_words = keyword.split()
    all_wodrs = []
    for word in keyword_words:
        all_wodrs.append(word)
        all_wodrs.append("ה" + word)
        all_wodrs.append("ל" + word)
        all_wodrs.append("מ" + word)
        all_wodrs.append("ש" + word)
        all_wodrs.append("כ" + word)
    final_search_term = " ".join(all_wodrs)
    queryString = text(
        "SELECT id, title, time, likes, user_id, age, kids, duration FROM peulas WHERE MATCH (title, content, targets, content_delta, targets_delta) AGAINST (:arg IN NATURAL LANGUAGE MODE);")
    rows = db.session.execute(queryString, {'arg': final_search_term}).fetchall()
    res = []
    for row in rows:
        res.append(Peula(
            id=row['id'],
            title=row['title'],
            time=row['time'],
            likes=row['likes'],
            user_id=row['user_id'],
            age=row['age'],
            kids=row['kids'],
            duration=row['duration']))
    return jsonify(list(map(lambda x: x.serializeShort(), set(res))))


@app.route('/peula_quick_smart_search/<string:keyword>/short')  # edit this too
def get_peula_by_Key_quick_smart_short(keyword):
    keyword_words = keyword.split()
    all_wodrs = []
    for word in keyword_words:
        all_wodrs.append(word)
        all_wodrs.append("ה" + word)
        all_wodrs.append("ל" + word)
        all_wodrs.append("מ" + word)
        all_wodrs.append("ש" + word)
        all_wodrs.append("כ" + word)
    final_search_term = " ".join(all_wodrs)
    queryString = text(
        "SELECT id, title, time, likes, user_id, age, kids, duration FROM peulas WHERE MATCH (title, targets, targets_delta) AGAINST (:arg IN NATURAL LANGUAGE MODE);")
    rows = db.session.execute(queryString, {'arg': final_search_term}).fetchall()
    res = []
    for row in rows:
        res.append(Peula(
            id=row['id'],
            title=row['title'],
            time=row['time'],
            likes=row['likes'],
            user_id=row['user_id'],
            age=row['age'],
            kids=row['kids'],
            duration=row['duration']))
    return jsonify(list(map(lambda x: x.serializeShort(), set(res))))


@app.route("/liked_puelas_by_user_page/<int:user>/<int:page>")
def get_liked_peulas_by_user_page(user, page):
    res = Peula.query.join(PeulaLike).filter(PeulaLike.user == user).order_by(PeulaLike.time.desc()).paginate(page, 3,
                                                                                                              False).items
    return jsonify(list(map(lambda x: x.serialize(), res)))


@app.route("/faved_puelas_by_user_page/<int:user>/<int:page>")
def faved_puelas_by_user_page(user, page):
    res = Peula.query.join(PeulaFav).filter(PeulaFav.user == user).order_by(PeulaFav.time.desc()).paginate(page, 3,
                                                                                                           False).items
    return jsonify(list(map(lambda x: x.serialize(), res)))


@app.route("/last_seen_puelas_by_user_page/<int:user>/<int:page>")
def get_last_seen_peulas_by_user_page(user, page):
    res = Peula.query.join(PeulaView).filter(PeulaView.user == user).order_by(PeulaView.time.desc()).paginate(page, 3,
                                                                                                              False).items
    return jsonify(list(map(lambda x: x.serialize(), res)))


#       S users
############################################

@app.route("/all_users/<string:passwd>")
def all_users(passwd):
    if passwd == "Bmn0tVz0cw3NCSe2I7CAXktjhRkdFwR9oBLISwFTjd14SCkmGhEmTw85SzB0gkNvvJTTmWpiCH7KbbupzyddCfSlj2mDXUFYUhfQ":
        return jsonify(list(map(lambda x: x.long_serialize(), User.query.all())))
    else:
        return "U R NOT ALLOWED"


@app.route("/name_by_id/<int:usr>")
def nameById(usr):
    usr = User.query.filter_by(id=usr).first()
    if usr is None:
        return "ERROR"
    else:
        return usr.nickname


@app.route("/user/<int:user_id>")
def user_by_id(user_id):
    usr = User.query.filter_by(id=user_id)
    if len(list(usr)) < 1:
        return "null"
    return jsonify(usr.first().serialize())


@app.route("/insert/user", methods=['POST'])
def addUser():
    content = request.json
    firstname = content['firstname']
    lastname = content['lastname']
    nickname = content['nickname']
    movment = content['movment']
    male = content['male']
    phone = content['phone']

    new = User(firstname=firstname, lastname=lastname,
               nickname=nickname, password="none", points=0, movment=movment, male=male, phone=phone)
    db.session.add(new)
    db.session.commit()
    newUser = User.query.filter_by(id=new.id).first()
    return jsonify(newUser.serialize())


@app.route("/username/<string:user_name>")
def user_by_name(user_name):
    usr = User.query.filter_by(nickname=user_name)
    if len(list(usr)) < 1:
        return jsonify("null")
    return jsonify(usr.first().serialize())


@app.route("/update_fname/<int:user_id>/<string:val>")
def updatefname(user_id, val):
    usr = User.query.filter_by(id=user_id).first()
    usr.firstname = val
    db.session.commit()
    return jsonify({'status': 200})


@app.route("/update_lname/<int:user_id>/<string:val>")
def updatelname(user_id, val):
    usr = User.query.filter_by(id=user_id).first()
    usr.lastname = val
    db.session.commit()
    return jsonify({'status': 200})


@app.route("/update_nname/<int:user_id>/<string:val>")
def updatenname(user_id, val):
    usr = User.query.filter_by(id=user_id).first()
    usr.nickname = val
    db.session.commit()
    return jsonify({'status': 200})


@app.route("/update_movment/<int:user_id>/<string:val>")
def update_movment(user_id, val):
    usr = User.query.filter_by(id=user_id).first()
    usr.movment = val
    db.session.commit()
    return jsonify({'status': 200})


@app.route("/update_male/<int:user_id>/<string:val>")
def update_male(user_id, val):
    usr = User.query.filter_by(id=user_id).first()
    usr.male = (val == "true")
    db.session.commit()
    return jsonify({'status': 200})


@app.route("/update_photopath/<int:user_id>/<string:val>")
def updatephotopath(user_id, val):
    usr = User.query.filter_by(id=user_id).first()
    usr.photopath = val
    db.session.commit()
    return jsonify({'status': 200})


@app.route("/update_connection/<int:user_id>")
def updateconnection(user_id):
    usr = User.query.filter_by(id=user_id).first()
    usr.last_connection_time = func.now()
    db.session.commit()
    return jsonify({'status': 200})


@app.route("/nickname_free/<string:nick>")
def returnNickExists(nick):
    return jsonify(User.query.filter_by(nickname=nick).first() == None)


@app.route("/games_by_user/<int:userId>")
def returnGamesByUser(userId):
    return jsonify(list(map(lambda x: x.serialize(), Game.query.filter_by(user_id=userId))))


@app.route("/peulas_by_user/<int:userId>")
def returnPeulasByUser(userId):
    return jsonify(list(map(lambda x: x.serialize(), Peula.query.filter_by(user_id=userId))))


@app.route("/user_by_phone/<string:phone>")
def returnUserByPhone(phone):
    res = User.query.filter_by(phone=phone).first()
    if res is not None:
        return jsonify(res.serialize())
    else:
        return jsonify(None)


@app.route("/post_user_connection", methods=['POST'])
def post_user_connection():
    content = request.json
    user = content['user']
    device_info = content['device_info']
    new_conn = UserConnection(user=user, device_info=device_info)
    db.session.add(new_conn)
    db.session.commit()
    return jsonify("OK")


#       S likes
############################################
@app.route("/like/<int:game_id>", methods=['POST'])
def like_a_game(game_id):
    game = Game.query.filter_by(id=game_id).first()
    game.likes = (game.likes + 1)
    publisher = User.query.filter_by(id=game.user_id).first()
    publisher.points = (publisher.points + 1)
    content = request.json
    user = content['user']
    newLike = GameLike(user=user, game=game_id)
    db.session.add(newLike)
    db.session.commit()
    return jsonify("{'status':200}")


@app.route("/likeP/<int:peula_id>", methods=['POST'])
def like_a_peula(peula_id):
    peula = Peula.query.filter_by(id=peula_id).first()
    peula.likes = (peula.likes + 1)
    publisher = User.query.filter_by(id=peula.user_id).first()
    publisher.points = (publisher.points + 2)
    content = request.json
    user = content['user']
    newLike = PeulaLike(user=user, peula=peula_id)
    db.session.add(newLike)
    db.session.commit()
    return jsonify("{'status':200}")


@app.route("/game_like/<int:like_id>")
def returnGameLikeById(like_id):
    return jsonify(GameLike.query.filter(GameLike.id == like_id).first().serialize())


@app.route("/peula_like/<int:like_id>")
def returnPeulaLikeById(like_id):
    return jsonify(PeulaLike.query.filter(PeulaLike.id == like_id).first().serialize())


@app.route("/game_likes_by_game/<int:game_id>")
def returnGameLikesByGameId(game_id):
    return jsonify(list(map(lambda x: x.serialize(), GameLike.query.filter(GameLike.game == game_id))))


@app.route("/peula_likes_by_peula/<int:peula_id>")
def returnPeulaLikesByPeulaId(peula_id):
    return jsonify(list(map(lambda x: x.serialize(), PeulaLike.query.filter(PeulaLike.game == peula_id))))


@app.route("/game_likes_by_user/<int:user_id>")
def returnGameLikesByUserId(user_id):
    x = GameLike.query.filter(GameLike.user == user_id)
    return jsonify(list(map(lambda x: x.serialize(), x)))


@app.route("/peula_likes_by_user/<int:user_id>")
def returnPeulaLikesByUserId(user_id):
    x = PeulaLike.query.filter(PeulaLike.user == user_id)
    return jsonify(list(map(lambda x: x.serialize(), x)))


#       S Favs
############################################
@app.route("/fav/<int:game_id>", methods=['POST'])
def fav_a_game(game_id):
    content = request.json
    user = content['user']
    newFav = GameFav(user=user, game=game_id)
    db.session.add(newFav)
    db.session.commit()
    return jsonify("{'status':200}")


@app.route("/favP/<int:peula_id>", methods=['POST'])
def fav_a_peula(peula_id):
    content = request.json
    user = content['user']
    newFav = PeulaFav(user=user, peula=peula_id)
    db.session.add(newFav)
    db.session.commit()
    return jsonify("{'status':200}")


@app.route("/game_fav/<int:fav_id>")
def returnGameFavById(fav_id):
    return jsonify(GameFav.query.filter(GameFav.id == fav_id).first().serialize())


@app.route("/peula_fav/<int:fav_id>")
def returnPeulaFavById(fav_id):
    return jsonify(PeulaFav.query.filter(PeulaFav.id == fav_id).first().serialize())


@app.route("/game_favs_by_game/<int:fav_id>")
def returnGameFavsByGameId(game_id):
    return jsonify(list(map(lambda x: x.serialize(), GameFav.query.filter(GameFav.game == game_id))))


@app.route("/peula_favs_by_peula/<int:peula_id>")
def returnPeulaFavsByPeulaId(peula_id):
    return jsonify(list(map(lambda x: x.serialize(), PeulaFav.query.filter(PeulaFav.game == peula_id))))


@app.route("/game_favs_by_user/<int:user_id>")
def returnGameFavsByUserId(user_id):
    x = GameFav.query.filter(GameFav.user == user_id)
    return jsonify(list(map(lambda x: x.serialize(), x)))


@app.route("/peula_favs_by_user/<int:user_id>")
def returnPeulaFavsByUserId(user_id):
    x = PeulaFav.query.filter(PeulaFav.user == user_id)
    return jsonify(list(map(lambda x: x.serialize(), x)))


#       S views and searches
############################################
@app.route("/insert/game_view/<int:game_id>/<int:user_id>")
def addGameView(game_id, user_id):
    new = GameView(game=game_id, user=user_id)
    db.session.add(new)
    db.session.commit()
    return "ok"


@app.route("/insert/peula_view/<int:peula_id>/<int:user_id>")
def addPeulaView(peula_id, user_id):
    new = PeulaView(peula=peula_id, user=user_id)
    db.session.add(new)
    db.session.commit()
    return "ok"


@app.route("/insert/peula_search/<string:search>/<int:user_id>")
def addPeulaSearch(search, user_id):
    new = PeulaSearch(search=search, user=user_id)
    db.session.add(new)
    db.session.commit()
    return "ok"


@app.route("/game_view_by_user/<int:id>")
def returnAllGameViewsByUser(id):
    return jsonify(list(map(lambda x: x.serialize(), GameView.query.filter(GameView.user == id))))


@app.route("/peula_view_by_user/<int:id>")
def returnAllPeulaViewsByUser(id):
    return jsonify(list(map(lambda x: x.serialize(), PeulaView.query.filter(PeulaView.user == id))))


@app.route("/peula_searches")
def returnPeulaSearches():
    res = db.session.query(PeulaSearch.search, func.count(PeulaSearch.search).label(
        'qty')).group_by(PeulaSearch.search).order_by(desc('qty')).all()
    return jsonify(res)
    # return (jsonify(list(map(lambda x: x.serialize(), res))))


@app.route("/peula_searches/<int:num>")
def returnPeulaSearchesLimited(num):
    res = db.session.query(PeulaSearch.search, func.count(PeulaSearch.search).label(
        'qty')).group_by(PeulaSearch.search).order_by(desc('qty')).limit(num).all()
    return jsonify(res)
    # return (jsonify(list(map(lambda x: x.serialize(), res))))


#       S icons
############################################
@app.route("/icon_list")
def icon_list():
    l = list()
    i = 0
    dirs = listdir("/home/boinxi/mysite/profile_pics/icons")
    for currDir in dirs:
        if path.isdir("/home/boinxi/mysite/profile_pics/icons/" + currDir):
            l.append(list())
            files = listdir(
                "/home/boinxi/mysite/profile_pics/icons/" + currDir)
            for currFile in files:
                l[i].append({"cat": currDir, "file": currFile})
            i = (i + 1)

    return jsonify(l)


@app.route("/icon/<string:cat>/<string:name>")
def icon(cat, name):
    p = "/home/boinxi/mysite/profile_pics/icons/" + cat + "/" + name
    cat.replace('..', 'amitamit')
    name.replace('..', 'amitamit')
    cat.replace('/', 'amitamit')
    name.replace('/', 'amitamit')
    return open(p, "r").read()


#       S suggestions and records
############################################
@app.route("/game_recos_page/<int:page>")
def gameRecosPage(page):
    ids = list(map(lambda x: x.game, list(GameReco.query.order_by(
        GameReco.time.desc(), GameReco.points.desc()).paginate(page, 3, False).items)))
    games = Game.query.filter(Game.id.in_(ids)).all()
    return jsonify(list(map(lambda x: x.serialize(), games)))


@app.route("/peula_recos_page/<int:page>")
def peulaRecosPage(page):
    ids = list(map(lambda x: x.peula, list(PeulaReco.query.order_by(
        PeulaReco.time.desc(), PeulaReco.points.desc()).paginate(page, 3, False).items)))
    peulas = Peula.query.filter(Peula.id.in_(ids)).all()
    return jsonify(list(map(lambda x: x.serialize(), peulas)))


@app.route("/new_peulas/<int:page>")
def newPeulasPage(page):
    return (jsonify(list(
        map(lambda x: x.serialize(), list(Peula.query.order_by(Peula.time.desc()).paginate(page, 1, False).items)))))


@app.route("/new_games/<int:page>")
def newGamesPage(page):
    return (jsonify(
        list(map(lambda x: x.serialize(), list(Game.query.order_by(Game.time.desc()).paginate(page, 1, False).items)))))


#       S comments
############################################
@app.route('/post_game_comment', methods=['POST'])
def addGameComment():
    content = request.json
    game = content['game']
    user = content['user']
    content = content['content']
    new = GameComment(game=game, user=user, content=content)
    db.session.add(new)
    db.session.commit()
    return "ok"


@app.route('/post_peula_comment', methods=['POST'])
def addPeulaComment():
    content = request.json
    peula = content['peula']
    user = content['user']
    content = content['content']
    new = PeulaComment(peula=peula, user=user, content=content)
    db.session.add(new)
    db.session.commit()
    return "ok"


@app.route('/get_comments_for_game/<int:game>')
def getCommentsForGame(game):
    return jsonify(list(map(lambda x: x.serialize(),
                            list(GameComment.query.filter(GameComment.game == game).order_by(GameComment.time.asc())))))


@app.route('/get_comments_for_peula/<int:peula>')
def getCommentsForPeula(peula):
    return jsonify(list(map(lambda x: x.serialize(), list(
        PeulaComment.query.filter(PeulaComment.peula == peula).order_by(PeulaComment.time.asc())))))


@app.route("/like_game_comment/<int:comment>")
def like_game_comment(comment):
    thisGameComment = GameComment.query.filter_by(id=comment).first()
    thisGameComment.likes = (thisGameComment.likes + 1)
    db.session.commit()
    return jsonify("{'status':200}")


@app.route("/like_peula_comment/<int:comment>")
def like_peula_comment(comment):
    thisPeulaComment = PeulaComment.query.filter_by(id=comment).first()
    thisPeulaComment.likes = (thisPeulaComment.likes + 1)
    db.session.commit()
    return jsonify("{'status':200}")


@app.route("/game_comment_count/<int:gameId>")
def game_comment_count(gameId):
    return str((db.session.query(func.count(GameComment.id)).filter(GameComment.game == gameId)).scalar())


@app.route("/peula_comment_count/<int:peulaId>")
def peula_comment_count(peulaId):
    return str((db.session.query(func.count(PeulaComment.id)).filter(PeulaComment.peula == peulaId)).scalar())


#       S sms
############################################
@app.route('/sms/get_code/<string:phone>')
def get_code(phone):
     api_url = "https://api.sms.to/sms/send?api_key=8JA7TdRiON8ECXfBEgjn8CdjSZzovJPH"
     new_code = random_with_N_digits(4)
     base_msg = "היי! הקוד שלך לקווה קווה הוא: "
     headers = {
         "to": phone,
         "message": base_msg + str(new_code),
         "sender_id": "qua qua"
     }
     res = json.loads(requests.get(api_url, json=headers).content)
     response = {
         "succ": res['success'],
         "code": new_code
     }
     return jsonify(response)
    #response = {
    #    "succ": True,
    #    "code": 1234
    #}



#       S web codes
############################################
@app.route('/get_web_code/<int:user_id>')
def web_code(user_id):
    new_code = random_with_N_digits(6)
    newCode = WebCode(user=user_id, code=new_code)
    db.session.add(newCode)
    db.session.commit()
    response = {
        "succ": True,
        "code": new_code
    }
    return jsonify(response)


@app.route('/find_user_for_web_code/<int:code>')
def find_web_code(code):
    thisWebCode = WebCode.query.filter_by(code=code).first()
    response = {
        "code": thisWebCode.code,
        "user": thisWebCode.user
    }
    return jsonify(response)


#       S general search
############################################
@app.route('/general_search/<string:keyword>')
def general_search(keyword):
    peulas = findPeulaByKeyQuick(keyword)

    games = []
    keywords = list(filter(lambda x: ((x != "") and (x != " ")), keyword.split(" ")))
    for keyword in keywords:
        games.extend(list(Game.query.filter(Game.title.like('%' + keyword + '%'))))
        games.extend(list(Game.query.filter(Game.description.like('%' + keyword + '%'))))

    users = []
    for keyword in keywords:
        users.extend(list(User.query.filter(User.firstname.like('%' + keyword + '%'))))
        users.extend(list(User.query.filter(User.lastname.like('%' + keyword + '%'))))
        users.extend(list(User.query.filter(User.nickname.like('%' + keyword + '%'))))

    res = {
        'peulas': list(map(lambda x: x.serialize(), list(peulas))),
        'games': list(map(lambda x: x.serialize(), list(set(games)))),
        'users': list(map(lambda x: x.serialize(), list(set(users)))),
    }
    return jsonify(res)


#       S reports
############################################
@app.route('/post_report', methods=['POST'])
def post_report():
    # [  "בחר",  "תוכן פוגעני או גזעני",  "ספאם",  "תוכן מיני",  "שימוש בקללות",  "תוכן פרסומי או שיווקי",  "שכנוע לאלימות או מעשים אובדניים",  "אחר- פרטו"]

    content = request.json

    post_id = content['post_id']
    game = content['game']
    type = content['type']
    description = content['description']
    sender_id = content['sender_id']
    new = Report(post_id=post_id, game=game, type=type, description=description, sender_id=sender_id)
    db.session.add(new)
    db.session.commit()
    return "ok"


#       S deletions
############################################
@app.route('/delete_game_like/<int:game_id>/<int:user>')
def delete_game_like(game_id, user):
    game = Game.query.filter_by(id=game_id).first()
    game.likes = (game.likes - 1)
    publisher = User.query.filter_by(id=game.user_id).first()
    publisher.points = (publisher.points - 1)
    like = GameLike.query.filter_by(user=user, game=game_id).first()
    new_deleted_like = DeletedGameLike.from_model(like)
    db.session.add(new_deleted_like)
    db.session.delete(like)
    db.session.commit()
    return jsonify("ok")


@app.route('/delete_peula_like/<int:peula_id>/<int:user>')
def delete_peula_like(peula_id, user):
    peula = Peula.query.filter_by(id=peula_id).first()
    peula.likes = (peula.likes - 1)
    publisher = User.query.filter_by(id=peula.user_id).first()
    publisher.points = (publisher.points - 2)
    like = PeulaLike.query.filter_by(user=user, peula=peula_id).first()
    new_deleted_like = DeletedPeulaLike.from_model(like)
    db.session.add(new_deleted_like)
    db.session.delete(like)
    db.session.commit()
    return jsonify("ok")


@app.route('/delete_game_comment/<int:comm_id>')
def delete_game_comment(comm_id):
    comm = GameComment.query.filter_by(id=comm_id).first()
    new_deleted_comm = DeletedGameComment.from_model(comm)
    db.session.add(new_deleted_comm)
    db.session.delete(comm)
    db.session.commit()
    return jsonify("ok")


@app.route('/delete_peula_comment/<int:comm_id>')
def delete_peula_comment(comm_id):
    comm = PeulaComment.query.filter_by(id=comm_id).first()
    new_deleted_comm = DeletedPeulaComment.from_model(comm)
    db.session.add(new_deleted_comm)
    db.session.delete(comm)
    db.session.commit()
    return jsonify("ok")


@app.route('/delete_peula_fav/<int:peula_id>/<int:user>')
def delete_peula_fav(peula_id, user):
    fav = PeulaFav.query.filter_by(user=user, peula=peula_id).first()
    new_deleted_fav = DeletedPeulaFav.from_model(fav)
    db.session.add(new_deleted_fav)
    db.session.delete(fav)
    db.session.commit()
    return jsonify("ok")


@app.route('/delete_game_fav/<int:game_id>/<int:user>')
def delete_game_fav(game_id, user):
    fav = GameFav.query.filter_by(user=user, game=game_id).first()
    new_deleted_fav = DeletedGameFav.from_model(fav)
    db.session.add(new_deleted_fav)
    db.session.delete(fav)
    db.session.commit()
    return jsonify("ok")


@app.route('/delete_game/<int:game_id>')
def delete_game(game_id):
    # delete likes (and remove points)
    all_likes = list(GameLike.query.filter_by(game=game_id).all())
    game = Game.query.filter_by(id=game_id).first()
    publisher = User.query.filter_by(id=game.user_id).first()
    publisher.points = (publisher.points - (1 * len(all_likes)))
    for like in all_likes:
        new_deleted_like = DeletedGameLike.from_model(like)
        db.session.add(new_deleted_like)
        db.session.delete(like)
    # delete views
    all_views = list(GameView.query.filter_by(game=game_id).all())
    for view in all_views:
        new_deleted_view = DeletedGameView.from_model(view)
        db.session.add(new_deleted_view)
        db.session.delete(view)
    # delete favs
    all_favs = list(GameFav.query.filter_by(game=game_id).all())
    for fav in all_favs:
        new_deleted_fav = DeletedGameFav.from_model(fav)
        db.session.add(new_deleted_fav)
        db.session.delete(fav)
    # delete reco
    all_recos = list(GameReco.query.filter_by(game=game_id).all())
    for reco in all_recos:
        db.session.delete(reco)
    # delete comments
    all_comments = list(GameComment.query.filter_by(game=game_id).all())
    for comm in all_comments:
        new_deleted_comm = DeletedGameComment.from_model(comm)
        db.session.add(new_deleted_comm)
        db.session.delete(comm)
    # delete game itself
    actual_game = Game.query.filter_by(id=game_id).first()
    deleted_game = DeletedGame.from_model(actual_game)
    db.session.add(deleted_game)
    db.session.delete(actual_game)

    db.session.commit()
    return "deleted."


@app.route('/delete_peula/<int:peula_id>')
def delete_peula(peula_id):
    # delete likes
    all_likes = list(PeulaLike.query.filter_by(peula=peula_id).all())
    peula = Peula.query.filter_by(id=peula_id).first()
    publisher = User.query.filter_by(id=peula.user_id).first()
    publisher.points = (publisher.points - (2 * len(all_likes)))
    for like in all_likes:
        new_deleted_like = DeletedPeulaLike.from_model(like)
        db.session.add(new_deleted_like)
        db.session.delete(like)
    # delete views
    all_views = list(PeulaView.query.filter_by(peula=peula_id).all())
    for view in all_views:
        new_deleted_view = DeletedPeulaView.from_model(view)
        db.session.add(new_deleted_view)
        db.session.delete(view)
    # delete favs
    all_favs = list(PeulaFav.query.filter_by(peula=peula_id).all())
    for fav in all_favs:
        new_deleted_fav = DeletedPeulaFav.from_model(fav)
        db.session.add(new_deleted_fav)
        db.session.delete(fav)
    # delete reco
    all_recos = list(PeulaReco.query.filter_by(peula=peula_id).all())
    for reco in all_recos:
        db.session.delete(reco)
    # delete comments
    all_comments = list(PeulaComment.query.filter_by(peula=peula_id).all())
    for comm in all_comments:
        new_deleted_comm = DeletedPeulaComment.from_model(comm)
        db.session.add(new_deleted_comm)
        db.session.delete(comm)
    # delete peula itself
    actual_peula = Peula.query.filter_by(id=peula_id).first()
    deleted_peula = DeletedPeula.from_model(actual_peula)
    db.session.add(deleted_peula)
    db.session.delete(actual_peula)

    db.session.commit()
    return "deleted."


#       S deprecater - for legacy
############################################
@app.route("/pic/<pic>")
def pic(pic):
    return send_file("profile_pics/" + str(pic), mimetype='image/gif')


@app.route("/pic_by_id/<int:usr>")
def picById(usr):
    usr = User.query.filter_by(id=usr).first()
    if usr is None:
        p_path = "profile_pics/default_profile_pic.png"
    else:
        p_path = "profile_pics/" + str(usr.id) + ".png"

    try:
        return send_file(p_path, mimetype='image/gif')
    except:
        return send_file("profile_pics/default_profile_pic.png", mimetype='image/gif')


@app.route("/random/game")
def get_random_game():
    random_game = Game.query.order_by(func.random()).first()
    return jsonify(random_game.serialize())


@app.route('/upload/<int:user_id>', methods=['POST'])
def upload_image(user_id):
    content = request.json
    img_string = content['image']
    fh = open("mysite/profile_pics/" + str(user_id) + ".png", "wb")
    fh.write(base64.b64decode(img_string))
    fh.close()
    return jsonify({'status': 'ok'})


#      S startup
############################################

@app.route("/startup")
def startup(): return jsonify({'show': False, 'allowApp': True,
                               'html': '<p>היי! אפליקציה זו בשלבי בניה ולא מוכנה עדיין.</p><p>אם את\ה לא אמור להיות פה, נבקש שלא תשתמש באפליקציה.</p><p>תודה</p><p><img src="https://media.giphy.com/media/3o6Ztl7oraKm4ZJ9mw/giphy.gif" width="498" height="362" /></p>'})


@app.route("/startup/<string:ver>")
def startup_ver(ver):
    if ver == "0.1":
        return jsonify({'show': True, 'allowApp': False,
                        'html': '<p>היי! אפליקציה זו בשלבי בניה ולא מוכנה עדיין.</p><p>אם את\ה לא אמור להיות פה, נבקש שלא תשתמש באפליקציה.</p><p>תודה</p><p><img src="https://media.giphy.com/media/3o6Ztl7oraKm4ZJ9mw/giphy.gif" width="498" height="362" /></p>'})
    if ver == "0.2":
        return jsonify({'show': True, 'allowApp': True,
                        'html': '<p>היי! אפליקציה זו בשלבי בניה ולא מוכנה עדיין.</p><p>אם את\ה לא אמור להיות פה, נבקש שלא תשתמש באפליקציה.</p><p>תודה</p><p><img src="https://media.giphy.com/media/3o6Ztl7oraKm4ZJ9mw/giphy.gif" width="498" height="362" /></p>'})

    return jsonify({'show': False, 'allowApp': True, 'html': 'NO Content" /></p>'})


# show is if to stop at the 'startup' page. if false everything else is irrelavent. allow app is if to allow users to continue to app after they see the html.


#      S misc
############################################
@app.route("/")
def ok(): return "<h1>OK</h1>"


@app.route("/movment_list")
def movment_list():
    movments = ["לא משתייך לכלום", "צופים", "מכבי צעיר", "כנפיים של קרמבו", "מועדונצ'יק", "צהל", "משרד החינוך או מורים",
                "אחריי", "הנוער העובד והלומד", "משצים", "תנועת תרבות", "בני עקיבא", "אחר"]
    return jsonify(movments)


@app.route("/loaderio-fc387bf6b1f38c8ca7e92143b8f28630/")
def abxsd():
    return "loaderio-fc387bf6b1f38c8ca7e92143b8f28630"


if __name__ == '__main__':
    app.run(debug=True)
